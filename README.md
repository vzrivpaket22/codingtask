# codingtask
## Usage
Build project by mvn clean install.
After build *codingtask-jar-with-dependencies.jar* will be present in target folder.
It is runnable jar file.
To run this file and see result you should pass some system variables.
### Variables
**inputFilePath** - path to file that stores data with items and categories. After build input file is available under path ./target-classes/input2.txt. **Required parameter**  
**categoryFilePath** - path to file that stores data with categories. Each category should starts from new line. . After build categories file is available under path ./target-classes/categories.txt. **Required parameter**    
**frequencyActions** - categories and actions for them, that will be used to print item and frequency with some format. Value of this property should be like this "category:action,animals:moreThan,cars:itemCount". **Optional parameter**    
**uniqueActions** - categories and actions for them, that will be used to print itemwith some format. Value of this property should be like this "category:action,animals:simple,cars:md5,cars:simple...". **Optional parameter**  
**reverseUniqueActions** - same to uniqueActions but items in category will be rendered in reverse order. **Optional parameter**     

Available actions for unique items output:  
**simple** -> prints just item  
**md5** -> prints item with its md5 hash  

Available actions for items frequency output:  
**itemCount** -> prints item and its count like hourse:5  
**moreThan** -> prints item and yes/no yes if count more or equals than 3 like six:yes
  

For example - when follow command will be executed:  
 **java -jar -DinputFilePath="./test-classes/input2.txt" -DcategoryFilePath="./test-classes/categories.txt" -DfrequencyActions="numbers:itemCount,numbers:moreThan" -DuniqueActions="animals:simple,animals:md5" -DreverseUniqueActions="animals:md5" ./codingtask-jar-with-dependencies.jar**

I will get next output:  
NUMBERS:  
 six:2  
 one:2  
 seven:1  
 two:1  
 three:2  
NUMBERS:  
 six:no  
 one:no  
 seven:no  
 two:no  
 three:no  
ANIMALS:  
 cow  
 horse  
 moose  
 sheep  
ANIMALS:  
 · § cow (81566e986cf8cc685a05ac5b634af7f8)  
 · § horse (f1bdf5ed1d7ad7ede4e3809bd35644b0)  
 · § moose (2fe7cec3131fa9662906ecfb2eac8a49)  
 · § sheep (23ec24c5ca59000543cee1dfded0cbea)  
ANIMALS:  
 · § sheep (23ec24c5ca59000543cee1dfded0cbea)  
 · § moose (2fe7cec3131fa9662906ecfb2eac8a49)  
 · § horse (f1bdf5ed1d7ad7ede4e3809bd35644b0)  
 · § cow (81566e986cf8cc685a05ac5b634af7f8)  



  
