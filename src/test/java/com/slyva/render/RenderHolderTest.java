package com.slyva.render;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

@RunWith(JUnit4.class)
public class RenderHolderTest {

    private static final String EMPTY = "";

    private ByteArrayOutputStream mockedOutputStream = new ByteArrayOutputStream();
    private PrintStream originalOut = System.out;
    private RenderHolder target;

    @Before
    public void setUp() {
        target = new RenderHolder();
        System.setOut(new PrintStream(mockedOutputStream));
    }

    @After
    public void cleanUp() {
        System.setOut(originalOut);
    }

    @Test
    public void shouldUseEmptyUniqueRender() {
        target.getUniqueItemRender("nonSetRender").accept("asdasd");

        Assert.assertEquals(EMPTY, mockedOutputStream.toString());
    }

    @Test
    public void shouldUseEmptyFrequencyRender() {
        target.getItemFrequencyRender("nonSetRender").accept("asdasd", 0L);

        Assert.assertEquals(EMPTY, mockedOutputStream.toString());
    }

    @Test
    public void shouldUseDefaultUniqueRenders() {
        target.getUniqueItemRender("simple").accept("item");
        target.getUniqueItemRender("md5").accept("item");

        String expected = " item\n · § item (447b7147e84be512208dcc0995d67ebc)\n";

        Assert.assertEquals(expected, mockedOutputStream.toString());
    }

    @Test
    public void shouldUseDefaultFrequencyRenders() {
        target.getItemFrequencyRender("itemCount").accept("item", 10L);
        target.getItemFrequencyRender("moreThan").accept("item", 10L);

        String expected = " item:10\n item:yes\n";

        Assert.assertEquals(expected, mockedOutputStream.toString());
    }

    @Test
    public void shouldRemoveUniqueRender() {
        target.removeUniqueItemRender("simple");

        target.getUniqueItemRender("simple").accept("item");

        Assert.assertEquals(EMPTY, mockedOutputStream.toString());
    }

    @Test
    public void shouldRemoveFrequencyRender() {
        target.removeItemFrequencyRender("moreThan");

        target.getItemFrequencyRender("moreThan").accept("item", 10L);

        Assert.assertEquals(EMPTY, mockedOutputStream.toString());
    }

    @Test
    public void shouldAddUniqueRender() {
        target.putUniqueItemRender("new", System.out::print);

        target.getUniqueItemRender("new").accept("new");

        Assert.assertEquals("new", mockedOutputStream.toString());
    }

    @Test
    public void shouldAddFrequencyRender() {
        target.putItemFrequencyRender("new", (item, frequency) -> System.out.print(item + frequency));

        target.getItemFrequencyRender("new").accept("new", 10L);

        Assert.assertEquals("new10", mockedOutputStream.toString());
    }
}