package com.slyva.render;

import com.slyva.render.frequency.ItemCountRender;
import com.slyva.render.frequency.MoreThanRender;
import com.slyva.render.unique.MD5ItemRender;
import com.slyva.render.unique.SimpleItemRender;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

@RunWith(JUnit4.class)
public class RendersTest {

    private ByteArrayOutputStream mockedOutputStream = new ByteArrayOutputStream();
    private PrintStream originalOut = System.out;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(mockedOutputStream));
    }

    @After
    public void cleanUp() {
        System.setOut(originalOut);
    }

    @Test
    public void shouldPrintItemCount() {
        ItemCountRender itemCountRender = new ItemCountRender();
        itemCountRender.accept("item", 22L);

        String expected = " item:22\n";

        Assert.assertEquals(expected, mockedOutputStream.toString());
    }

    @Test
    public void shouldPrintMoreThan() {
        MoreThanRender moreThanRender = new MoreThanRender();
        moreThanRender.accept("0", 22L);

        String expected = " 0:yes\n";

        Assert.assertEquals(expected, mockedOutputStream.toString());
    }

    @Test
    public void shouldPrintItem() {
        SimpleItemRender simpleItemRender = new SimpleItemRender();
        simpleItemRender.accept("item");

        String expected = " item\n";

        Assert.assertEquals(expected, mockedOutputStream.toString());
    }

    @Test
    public void shouldPrintMd5() {
        MD5ItemRender md5ItemRender = new MD5ItemRender();
        md5ItemRender.accept("item");

        String expected = " · § item (447b7147e84be512208dcc0995d67ebc)\n";

        Assert.assertEquals(expected, mockedOutputStream.toString());
    }
}