package com.slyva;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

@RunWith(JUnit4.class)
public class DemoApplicationTest {

    private static final String INPUT_FILE_PATH_SYSTEM_PROPERTY = "inputFilePath";
    private static final String CATEGORY_FILE_PATH_SYSTEM_PROPERTY = "categoryFilePath";
    private static final String UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME = "uniqueActions";
    private static final String REVERSE_UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME = "reverseUniqueActions";
    private static final String FREQUENCY_ACTIONS_SYSTEM_PROPERTY_NAME = "frequencyActions";

    private ByteArrayOutputStream mockedOutputStream = new ByteArrayOutputStream();
    private PrintStream originalOut = System.out;

    @Before
    public void setUp() {
        System.setProperty(INPUT_FILE_PATH_SYSTEM_PROPERTY,
                           new File(getClass().getClassLoader().getResource("input2.txt").getFile()).getPath());
        System.setProperty(CATEGORY_FILE_PATH_SYSTEM_PROPERTY,
                           new File(getClass().getClassLoader().getResource("categories.txt").getFile()).getPath());
        System.setOut(new PrintStream(mockedOutputStream));
    }

    @After
    public void cleanUp() {
        System.setOut(originalOut);
        System.clearProperty(INPUT_FILE_PATH_SYSTEM_PROPERTY);
        System.clearProperty(CATEGORY_FILE_PATH_SYSTEM_PROPERTY);
    }

    @Test
    public void shouldOutputNothing() throws IOException {
        DemoApplication.main(null);

        Assert.assertEquals("", mockedOutputStream.toString());
    }

    @Test
    public void shouldOutputDataFromTestFiles() throws IOException {
        System.setProperty(UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME, "animals:simple");
        System.setProperty(REVERSE_UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME, "cars:md5");
        System.setProperty(FREQUENCY_ACTIONS_SYSTEM_PROPERTY_NAME, "numbers:itemCount");

        DemoApplication.main(null);

        System.clearProperty(UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME);
        System.clearProperty(REVERSE_UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME);
        System.clearProperty(FREQUENCY_ACTIONS_SYSTEM_PROPERTY_NAME);

        String expected = "NUMBERS:\n six:2\n one:2\n seven:1\n two:1\n three:2\n"
                          + "ANIMALS:\n cow\n horse\n moose\n sheep\n"
                          + "CARS:\n"
                          + " · § vw (7336a2c49b0045fa1340bf899f785e70)\n"
                          + " · § opel (f65b7d39472c52142ea2f4ea5e115d59)\n"
                          + " · § bmw (71913f59e458e026d6609cdb5a7cc53d)\n"
                          + " · § audi (4d9fa555e7c23996e99f1fb0e286aea8)\n";

        Assert.assertEquals(expected, mockedOutputStream.toString());
    }
}