package com.slyva.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class SystemDataProviderTest {

    @Test
    public void shouldReturnParsedLineWithSeveralCategoryActionsLine() {
        String categoryActionLine = "category:action,animals:md5,cars:simple,numbers:reverse";
        List<AbstractMap.SimpleEntry<String, String>> actualResult = SystemDataProvider.getActions(categoryActionLine);

        List<AbstractMap.SimpleEntry<String, String>> expectedResult = new ArrayList<>();
        expectedResult.add(new AbstractMap.SimpleEntry<>("category", "action"));
        expectedResult.add(new AbstractMap.SimpleEntry<>("animals", "md5"));
        expectedResult.add(new AbstractMap.SimpleEntry<>("cars", "simple"));
        expectedResult.add(new AbstractMap.SimpleEntry<>("numbers", "reverse"));

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void shouldReturnMapWithOneCategoryActionLine() {
        String categoryActionLine = "category:action";
        List<AbstractMap.SimpleEntry<String, String>> actualResult = SystemDataProvider.getActions(categoryActionLine);

        List<AbstractMap.SimpleEntry<String, String>> expectedResult = new ArrayList<>();
        expectedResult.add(new AbstractMap.SimpleEntry<>("category", "action"));

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void shouldReturnMapWithOneCategoryActionWhenSeparatorForSecondActionIsIncorrect() {
        String categoryActionLine = "category:action,categ";
        List<AbstractMap.SimpleEntry<String, String>> actualResult = SystemDataProvider.getActions(categoryActionLine);

        List<AbstractMap.SimpleEntry<String, String>> expectedResult = new ArrayList<>();
        expectedResult.add(new AbstractMap.SimpleEntry<>("category", "action"));

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void shouldReturnEmptyMapWhenInputIsIncorrect() {
        String categoryActionLine = "categoryactioncateg, asdasdasdasd";
        List<AbstractMap.SimpleEntry<String, String>> actualResult = SystemDataProvider.getActions(categoryActionLine);

        List<AbstractMap.SimpleEntry<String, String>> expectedResult = new ArrayList<>();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void shouldReadLinesFromFile() throws IOException {
        List<String> testLines = Arrays.asList("1", "2");
        File testInputFile = File.createTempFile("testInput", null);
        Files.write(Paths.get(testInputFile.getPath()), testLines);

        List<String> actual = SystemDataProvider.getInputLines(testInputFile.getPath());

        Assert.assertEquals(testLines, actual);
        testInputFile.deleteOnExit();
    }
}