package com.slyva.util;

import static org.mockito.Mockito.when;

import com.slyva.processor.CategoryHolder;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

@RunWith(MockitoJUnitRunner.class)
public class CategoryHolderUtilTest {

    private static final String TEST_CATEGORY = "animals";
    private static final String[] TEST_ITEMS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    @Mock
    private CategoryHolder categoryHolder;

    @Test
    public void consumerShouldBeAppliedInNaturalOrder() {
        when(categoryHolder.getUniqueSortedItemsByCategoryName(TEST_CATEGORY)).thenReturn(Arrays.asList(TEST_ITEMS));

        List<String> actualResult = new ArrayList<>();
        CategoryHolderUtil.processSortedUniqueItems(categoryHolder, TEST_CATEGORY, actualResult::add);

        List<String> expected = Arrays.asList(TEST_ITEMS);

        Assert.assertEquals(expected, actualResult);
    }

    @Test
    public void consumerShouldBeAppliedInReverseOrder() {
        when(categoryHolder.getUniqueSortedItemsByCategoryName(TEST_CATEGORY)).thenReturn(Arrays.asList(TEST_ITEMS));

        List<String> actualResult = new ArrayList<>();
        CategoryHolderUtil.processReverseSortedUniqueItems(categoryHolder, TEST_CATEGORY, actualResult::add);

        List<String> expected = Arrays.asList(TEST_ITEMS);
        Collections.reverse(expected);

        Assert.assertEquals(expected, actualResult);
    }

    @Test
    public void biConsumerShouldBeApplied() {
        Map<String, Long> testMap = new HashMap<>();
        testMap.put("dog", 10L);
        testMap.put("cat", 20L);
        testMap.put("pixy", 30L);

        when(categoryHolder.getUniqueItemsWithFrequencyMapByCategoryName(TEST_CATEGORY)).thenReturn(testMap);

        Set<String> actualAnimals = new HashSet<>();
        AtomicLong actualTotalCount = new AtomicLong();
        CategoryHolderUtil.processItemsFrequencyMap(categoryHolder, TEST_CATEGORY, (animal, count) -> {
            actualAnimals.add(animal);
            actualTotalCount.addAndGet(count);
        });

        AtomicLong expectedTotalCount = new AtomicLong(60L);
        Set<String> expectedAnimals = new HashSet<>(Arrays.asList("dog", "cat", "pixy"));

        Assert.assertEquals(expectedAnimals, actualAnimals);
        Assert.assertEquals(expectedTotalCount.get(), actualTotalCount.get());
    }
}