package com.slyva.processor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@RunWith(JUnit4.class)
public class CategoryHolderTest {

    private static final String[] CATEGORIES = {"numbers", "animals"};
    private static final String NON_EXISTING_CATEGORY = "non_existing_category";

    private CategoryHolder target;

    @Before
    public void setUp() throws IOException {
        File fileWithTestData = new File(getClass().getClassLoader().getResource("input.txt").getFile());
        List<String> inputData = Files.readAllLines(Paths.get(fileWithTestData.getPath()));
        System.out.println(fileWithTestData.getPath());
        target = new CategoryHolder(inputData, new HashSet<>(Arrays.asList(CATEGORIES)));
    }

    @Test
    public void shouldReturnSortedListWithUniqueItems() {
        List<String> actualAnimals = target.getUniqueSortedItemsByCategoryName("animals");
        List<String> actualNumbers = target.getUniqueSortedItemsByCategoryName("numbers");

        List<String> expectedAnimals = Arrays.asList("cow", "horse", "moose", "sheep");
        List<String> expectedNumbers = Arrays.asList("one", "seven", "six", "three", "two");

        Assert.assertEquals(expectedAnimals, actualAnimals);
        Assert.assertEquals(expectedNumbers, actualNumbers);
    }

    @Test
    public void shouldReturnFrequencyMapWithItems() {
        target.addCategory("sheep");
        target.remapItemsToCategories();
        List<String> actualSheeps = target.getUniqueSortedItemsByCategoryName("sheep");
        List<String> actualAnimals = target.getUniqueSortedItemsByCategoryName("animals");

        List<String> expextedSheeps = Arrays.asList("cow", "horse");
        List<String> expectedAnimals = Collections.singletonList("moose");

        Assert.assertEquals(expextedSheeps, actualSheeps);
        Assert.assertEquals(expectedAnimals, actualAnimals);
    }

    @Test
    public void shouldSetAndRebuildItems() {
        target.removeCategory("animals");
        target.remapItemsToCategories();

        List<String> actualAnimals = target.getUniqueSortedItemsByCategoryName("animals");
        List<String> actualNumbers = target.getUniqueSortedItemsByCategoryName("numbers");

        List<String> expectedNumbers = Arrays.asList("animals", "cow", "horse", "moose", "one", "seven", "sheep", "six", "three", "two");

        Assert.assertEquals(Collections.emptyList(), actualAnimals);
        Assert.assertEquals(expectedNumbers, actualNumbers);
    }

    @Test
    public void shouldReturnEmptyUniqueCategoryItemsWhenCategoryNotExist() {
        Assert.assertEquals(Collections.emptyList(), target.getUniqueSortedItemsByCategoryName(NON_EXISTING_CATEGORY));
    }

    @Test
    public void shouldReturnEmptyItemsFrequencyWhenCategoryNotExist() {
        Assert.assertEquals(Collections.emptyMap(), target.getUniqueItemsWithFrequencyMapByCategoryName(NON_EXISTING_CATEGORY));
    }

}