package com.slyva.render;

import com.slyva.render.frequency.ItemCountRender;
import com.slyva.render.frequency.MoreThanRender;
import com.slyva.render.unique.MD5ItemRender;
import com.slyva.render.unique.SimpleItemRender;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Stores renders for unique items and item frequency items.
 */
public class RenderHolder {

    private static final Consumer<String> UNIQUE_NO_ACTION_RENDER = (item) -> {};
    private static final BiConsumer<String, Long> ITEM_FREQUENCY_NO_ACTION_RENDER = (item, frequency) -> {};
    private static final String MORE_THAN_DEFAULT_ACTION = "moreThan";
    private static final String ITEM_COUNT_DEFAULT_ACTION = "itemCount";
    private static final String MD_5_DEFAULT_ACTION = "md5";
    private static final String SIMPLE_DEFAULT_ACTION = "simple";

    private Map<String, Consumer<String>> uniqueItemRenders = new HashMap<>();
    private Map<String, BiConsumer<String, Long>> itemFrequencyRenders = new HashMap<>();

    /**
     * By default render stores the follow renders:
     *      unique: simple, md5
     *      frequency: itemCount, moreThan
     */
    public RenderHolder() {
        putUniqueItemRender(SIMPLE_DEFAULT_ACTION, new SimpleItemRender());
        putUniqueItemRender(MD_5_DEFAULT_ACTION, new MD5ItemRender());
        putItemFrequencyRender(ITEM_COUNT_DEFAULT_ACTION, new ItemCountRender());
        putItemFrequencyRender(MORE_THAN_DEFAULT_ACTION, new MoreThanRender());
    }

    /**
     * Add new unique render.
     *
     * @param renderName used for store some render.
     * @param render that will be associated with @renderName
     */
    public void putUniqueItemRender(String renderName, Consumer<String> render) {
        uniqueItemRenders.put(renderName, render);
    }

    /**
     * Retrieves unique item render that associated with some name.
     *
     * @param renderName used for find some render.
     * @return render that was found. If render was not found then empty render will be returned.
     */
    public Consumer<String> getUniqueItemRender(String renderName) {
        return uniqueItemRenders.getOrDefault(renderName, UNIQUE_NO_ACTION_RENDER);
    }

    /**
     * Removes unique render from holder.
     *
     * @param renderName used for removing render that associated with this name.
     */
    public void removeUniqueItemRender(String renderName) {
        uniqueItemRenders.remove(renderName);
    }

    /**
     * Add new item frequency render.
     *
     * @param renderName used for store some render.
     * @param render that will be associated with @renderName
     */
    public void putItemFrequencyRender(String renderName, BiConsumer<String, Long> render) {
        itemFrequencyRenders.put(renderName, render);
    }

    /**
     * Retrieves item frequency render that associated with some name.
     *
     * @param renderName used for find some render.
     * @return render that was found. If render was not found then empty render will be returned.
     */
    public BiConsumer<String, Long> getItemFrequencyRender(String renderName) {
        return itemFrequencyRenders.getOrDefault(renderName, ITEM_FREQUENCY_NO_ACTION_RENDER);
    }

    /**
     * Removes item frequency render from holder.
     *
     * @param renderName used for removing render that associated with this name.
     */
    public void removeItemFrequencyRender(String renderName) {
        itemFrequencyRenders.remove(renderName);
    }

}
