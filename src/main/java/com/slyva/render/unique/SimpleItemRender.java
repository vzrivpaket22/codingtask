package com.slyva.render.unique;

import java.util.function.Consumer;

public class SimpleItemRender implements Consumer<String> {

    @Override
    public void accept(String item) {
        System.out.printf(" %s\n", item);
    }
}
