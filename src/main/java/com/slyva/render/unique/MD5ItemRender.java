package com.slyva.render.unique;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.function.Consumer;

public class MD5ItemRender implements Consumer<String> {

    @Override
    public void accept(String item) {
        System.out.printf(" · § %s (%s)\n", item, DigestUtils.md5Hex(item));
    }
}
