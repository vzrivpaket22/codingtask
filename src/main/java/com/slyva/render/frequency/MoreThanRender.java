package com.slyva.render.frequency;

import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class MoreThanRender implements BiConsumer<String, Long> {

    private long minimum = 3L;

    public MoreThanRender() {
    }

    public MoreThanRender(long minimum) {
        this.minimum = minimum;
    }

    @Override
    public void accept(String item, Long count) {
        String isMore = Stream.of(count)
                .filter(itemCount -> itemCount >= minimum)
                .findFirst()
                .map(itemCount -> "yes")
                .orElse("no");
        System.out.printf(" %s:%s\n", item, isMore);
    }
}
