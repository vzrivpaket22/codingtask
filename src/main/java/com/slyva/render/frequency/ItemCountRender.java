package com.slyva.render.frequency;

import java.util.function.BiConsumer;

public class ItemCountRender implements BiConsumer<String, Long> {

    @Override
    public void accept(String item, Long count) {
        System.out.printf(" %s:%d\n", item, count);
    }
}
