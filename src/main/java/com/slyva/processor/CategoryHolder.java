package com.slyva.processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CategoryHolder {

    private Map<String, List<String>> categoriesWithItems;
    private List<String> dataLines = new ArrayList<>();
    private Set<String> categories = new HashSet<>();

    /**
     * Construct holder that group initial data by categories.
     *
     * @param inputLines data with categories and items
     *                          Data will be processed and collected to appropriate structure.
     * @param initialCategories the set of categories. Initial data will be divided by this categories.
     *                          Note: each category will be lower cased.
     */
    public CategoryHolder(List<String> inputLines, Set<String> initialCategories) {
        dataLines.addAll(inputLines);
        Set<String> lowerCaseCategorySet = initialCategories.stream()
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
        categories.addAll(lowerCaseCategorySet);
        categoriesWithItems = getCategoriesWithItemsFromInputLines(dataLines, categories);
    }

    /**
     * Retrieve all unique elements by particular category in natural order/
     *
     * @param categoryName will be used to filter items.
     *                     If current holder does not contain passed category then empty list with items will be returned.
     * @return natural sorted list that contains unique elements that belong to category with name categoryName.
     */
    public List<String> getUniqueSortedItemsByCategoryName(String categoryName) {
        return Optional.ofNullable(categoriesWithItems.get(categoryName))
                .orElse(Collections.emptyList())
                .stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * Collects map that contains item and its count in particular category
     *
     * @param categoryName will be used to filter items.
     *                     If current holder does not contain passed category then empty map with items will be returned.
     * @return map with item as a key, and count of item occurrence as a value.
     */
    public Map<String, Long> getUniqueItemsWithFrequencyMapByCategoryName(String categoryName) {
        return Optional.ofNullable(categoriesWithItems.get(categoryName))
                .orElse(Collections.emptyList())
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    /**
     * Adds new category. It will allow rebuild data, and items will be remapped to categories with new category.
     * To rebuild data, please use #remapItemsToCategories.
     *
     * @param categoryName new category name.
     */
    public void addCategory(String categoryName) {
        this.categories.add(categoryName.toLowerCase());
    }

    /**
     * Removes category from categories. It will allow rebuild data, and items will be remapped to categories without removed category.
     * rebuild data, please use #remapItemsToCategories.
     *
     * @param categoryName to remove.
     */
    public void removeCategory(String categoryName) {
        this.categories.remove(categoryName);
    }

    /**
     * Rebuilds input lines. Map items to categories.
     * Categories can be changed and after that rebuild can be necessary.
     */
    public void remapItemsToCategories() {
        this.categoriesWithItems = getCategoriesWithItemsFromInputLines(this.dataLines, this.categories);
    }

    /**
     * Processes lines with data. Processed lines will be converted to map and divided by some categories.
     *
     * @param lines      data that will be processed and divided.
     * @param categories is a mark to divide items/
     * @return map with category as a key and items of category as a value.
     */
    protected Map<String, List<String>> getCategoriesWithItemsFromInputLines(List<String> lines, Set<String> categories) {
        Map<String, List<String>> collectedDataByCategories = new HashMap<>();
        categories.forEach(category -> collectedDataByCategories.put(category, new ArrayList<>()));

        AtomicReference<String> currentCategory = new AtomicReference<>();
        for (String line : lines) {
            String lowerCaseLine = line.toLowerCase();
            Optional.ofNullable(collectedDataByCategories.get(lowerCaseLine))
                    .ifPresent(categoryList -> currentCategory.set(lowerCaseLine));
            Optional.ofNullable(currentCategory.get())
                    .filter(category -> Objects.isNull(collectedDataByCategories.get(lowerCaseLine)))
                    .ifPresent(category -> collectedDataByCategories.get(currentCategory.get()).add(lowerCaseLine));
        }

        return collectedDataByCategories;
    }

}
