package com.slyva;

import com.slyva.processor.CategoryHolder;
import com.slyva.render.RenderHolder;
import com.slyva.util.CategoryHolderUtil;
import com.slyva.util.SystemDataProvider;

import java.io.IOException;
import java.util.HashSet;

/**
 * Class that uses business logic to demonstrate some functionality and ability of written code.
 */
public class DemoApplication {

    private static final String INPUT_FILE_PATH_SYSTEM_PROPERTY_NAME = "inputFilePath";
    private static final String FREQUENCY_ACTIONS_SYSTEM_PROPERTY_NAME = "frequencyActions";
    private static final String UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME = "uniqueActions";
    private static final String REVERSE_UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME = "reverseUniqueActions";
    private static final String CATEGORY_FILE_PATH_SYSTEM_PROPERTY_NAME = "categoryFilePath";

    public static void main(String[] args) throws IOException {
        RenderHolder renderHolder = new RenderHolder();
        CategoryHolder categoryHolder =
                new CategoryHolder(SystemDataProvider.getInputLines(System.getProperty(INPUT_FILE_PATH_SYSTEM_PROPERTY_NAME)),
                                   new HashSet<>(SystemDataProvider.getInputLines(System.getProperty(CATEGORY_FILE_PATH_SYSTEM_PROPERTY_NAME))));

        SystemDataProvider.getActions(System.getProperty(FREQUENCY_ACTIONS_SYSTEM_PROPERTY_NAME))
                .forEach(categoryActionEntry -> CategoryHolderUtil
                        .processItemsFrequencyMap(categoryHolder, categoryActionEntry.getKey(),
                                                  renderHolder.getItemFrequencyRender(categoryActionEntry.getValue())));

        SystemDataProvider.getActions(System.getProperty(UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME))
                .forEach(categoryActionEntry -> CategoryHolderUtil
                        .processSortedUniqueItems(categoryHolder, categoryActionEntry.getKey(),
                                                  renderHolder.getUniqueItemRender(categoryActionEntry.getValue())));

        SystemDataProvider.getActions(System.getProperty(REVERSE_UNIQUE_ACTIONS_SYSTEM_PROPERTY_NAME))
                .forEach(categoryActionEntry -> CategoryHolderUtil
                        .processReverseSortedUniqueItems(categoryHolder, categoryActionEntry.getKey(),
                                                         renderHolder.getUniqueItemRender(categoryActionEntry.getValue())));
    }
}
