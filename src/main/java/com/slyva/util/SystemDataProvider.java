package com.slyva.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Responsible to convert and process string data to objects of some type.
 * First example read file and return lines by file path.
 * Second example process some string with separators and convert it into map.
 */
public class SystemDataProvider {

    private static final String EMPTY = "";
    private static final String COMMA = ",";
    private static final String CATEGORY_ACTION_SPLITTER = ":";
    private static final int CATEGORY_ACTION_SPLITTED_ARRAY_SIZE = 2;

    /**
     * Process value of system variable that stores data with categories and actions of categories.
     * Value of system property should
     *
     * @param categoryActionString sting with category and actions. To correct converting to map this string should have follow format
     *                             category:action,animals:md5,cars:simple,numbers:reverse,...
     * @return map with category as a key and value as an action.
     */
    public static List<AbstractMap.SimpleEntry<String, String>> getActions(String categoryActionString) {
        return Arrays.stream(Optional.ofNullable(categoryActionString).orElse(EMPTY).split(COMMA))
                .map(categoryAction -> categoryAction.split(CATEGORY_ACTION_SPLITTER))
                .filter(categoryAction -> categoryAction.length == CATEGORY_ACTION_SPLITTED_ARRAY_SIZE)
                .map(categoryAction -> new AbstractMap.SimpleEntry<>(categoryAction[0], categoryAction[1]))
                .collect(Collectors.toList());
    }

    /**
     * Utility functionality that reads file and returns lines of file.
     *
     * @param filePath to get lines.
     * @return list lines of file.
     * @throws IOException if some problem with file.
     */
    public static List<String> getInputLines(String filePath) throws IOException {
        return Files.readAllLines(Paths.get(filePath));
    }
}
