package com.slyva.util;

import com.slyva.processor.CategoryHolder;

import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CategoryHolderUtil {

    /**
     * Applies some consume function to each item in natural order. Items will be retrieved by some category.
     *
     * @param categoryHolder categories and items store
     * @param categoryName   category to retrieve items from categoryHolder
     * @param processor      will be applied to each item of founded items by category.
     */
    public static void processSortedUniqueItems(CategoryHolder categoryHolder, String categoryName, Consumer<String> processor) {
        printCategoryName(categoryName);
        categoryHolder.getUniqueSortedItemsByCategoryName(categoryName)
                .forEach(processor);
    }

    /**
     * Applies some consume function to each item in reverse order. Items will be retrieved by some category.
     *
     * @param categoryHolder categories and items store
     * @param categoryName   category to retrieve items from categoryHolder
     * @param processor      will be applied to each item of founded items by category.
     */
    public static void processReverseSortedUniqueItems(CategoryHolder categoryHolder, String categoryName, Consumer<String> processor) {
        printCategoryName(categoryName);
        categoryHolder.getUniqueSortedItemsByCategoryName(categoryName)
                .stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList())
                .forEach(processor);
    }

    /**
     * Applies some consume function to each pair item and frequency. Pairs will be retrieved by some category.
     *
     * @param categoryHolder categories and items store
     * @param categoryName   category to retrieve items from categoryHolder
     * @param processor      will be applied to each pair of founded pairs by category.
     */
    public static void processItemsFrequencyMap(CategoryHolder categoryHolder, String categoryName, BiConsumer<String, Long> processor) {
        printCategoryName(categoryName);
        categoryHolder.getUniqueItemsWithFrequencyMapByCategoryName(categoryName)
                .forEach(processor);
    }

    private static void printCategoryName(String categoryName) {
        System.out.printf("%s:\n", categoryName.toUpperCase());
    }

}
